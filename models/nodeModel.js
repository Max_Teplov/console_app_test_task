'use strict'

/*
Created by Max Teplov on 29/6/2017
*/

let node = function(obj) {

    return {
        name: obj.name,
        children: [],
        parent_id: obj.parent_id || null
    }
}

module.exports.node = node
