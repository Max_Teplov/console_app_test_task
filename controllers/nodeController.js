'use strict'

/*
Created by Max Teplov on 29/6/2017
*/

let Node = require('../models/nodeModel').node,
    nodesList = {};

exports.addNode = function(payload) {
    console.log('addnode payload->', payload)
    if (!(payload.id in nodesList)) {

        let node = new Node(payload)

        

        if (payload.parent_id) {

            if ((nodesList[payload.parent_id]) && (nodesList[payload.parent_id].children.indexOf(payload.id) === -1)) {

            	//serching for siblings with the same name
                if (nodesList[payload.parent_id].children.length) {

                    for (let idind in nodesList[payload.parent_id].children) {
                    	let nodeID = nodesList[payload.parent_id].children[idind]
                    	console.log('nodeID->',nodeID,'nodesList[nodeID]->',nodesList[nodeID] ) 
                        if (nodesList[nodeID].name === payload.name) {
                            throw 'there is already exists sibling node with name ' + payload.name
                        }
                    }
                }



                nodesList[payload.parent_id].children.push(payload.id)
                console.log('\nnodesList->', nodesList)

                // return process.stdout.write('{"ok":true}\n\n');

            } else {
                console.log('\nnodesList->', nodesList)

                return process.stdout.write('{"ok":false}\n\n');

            }

        }

        nodesList[payload.id] = node;

        console.log('\nnodesList->', nodesList)

        process.stdout.write('{"ok":true}\n\n');
    } else {
        process.stdout.write('{"ok":false}\n\n');
    }
}

exports.deleteNode = function(payload) {

    console.log('deleteNode->', payload)

    if (nodesList[payload.id]) {

        if (!nodesList[payload.id].children.length) {

            let parentId = nodesList[payload.id].parent_id

            if (parentId) {

                let removedChild = nodesList[parentId].children.splice(nodesList[parentId].children.indexOf(payload.id))

                console.log('Node with id ', removedChild, ' removed from childList of node with id ', parentId)
            }

            delete nodesList[payload.id]

            return process.stdout.write('{"ok":true}\n\n');

        } else {
            console.log('node with id ', payload.id, ' has children, so it can not be removed!')
            return process.stdout.write('{"ok":false}\n\n');
        }


    } else {
        console.log('there is no node with id', payload.id)
        return process.stdout.write('{"ok":false}\n\n');

    }
}


exports.moveNode = function(payload) {

    if (nodesList[payload.id] && nodesList[payload.new_parent_id]) {





        if (nodesList[payload.id].children.indexOf(payload.new_parent_id) === -1) {






        	//serching for siblings with the same name
        	if (nodesList[payload.new_parent_id].children.length) {

                    for (let idind in nodesList[payload.new_parent_id].children) {

                    	let nodeID = nodesList[payload.new_parent_id].children[idind]
                    	console.log('nodeID->',nodeID,'nodesList[nodeID]->',nodesList[nodeID] ) 
                        if (nodesList[nodeID].name === payload.name) {
                            throw 'there is already exists sibling node with name ' + payload.name
                        }
                    }
                }





            let parentId = nodesList[payload.id].parent_id

            if (parentId) {

                let removedChild = nodesList[parentId].children.splice(nodesList[parentId].children.indexOf(payload.id))

                console.log('Node with id ', removedChild, ' removed from childList of node with id ', parentId)
            }

            nodesList[payload.id].parent_id = payload.new_parent_id

            nodesList[payload.new_parent_id].children.push(payload.id)

            return process.stdout.write('{"ok":true}\n\n');

        } else {
            console.log('you can not create cycle links')
            return process.stdout.write('{"ok":false}\n\n');
            
        }

    } else {
        console.log('there is no node with id', payload.id, ' or ', payload.new_parent_id)
        return process.stdout.write('{"ok":false}\n\n');

    }

}

exports.query = function(payload) {

    let nodes = []

    let depthLevel = 0



    let getDepthLevel = function(nodeId) {

        if (('parent_id' in nodesList[nodeId]) && (nodesList[nodeId].parent_id)) {

            depthLevel = depthLevel + 1
            return getDepthLevel(nodesList[nodeId].parent_id)

        } else {

            let currentLevel = depthLevel
            depthLevel = 0
            return currentLevel
        }
    }

    for (let node in nodesList) {

        let nodeItem = nodesList[node]

        nodeItem.id = node
        nodes.push(nodeItem)
    }

    if (('names' in payload) && payload.names instanceof Array) {
        let filteredNodes = nodes.filter(function(node) {
            if (payload.names.indexOf(node.name) > -1) {
                return true
            }
        })

        nodes = filteredNodes

    }

    if ('max_depth' in payload) {

        let filteredNodes = nodes.filter(function(node) {
            let level = getDepthLevel(node.id)

            if (level <= payload.max_depth) {
                return true
            }

        })

        nodes = filteredNodes


    }

    if ('min_depth' in payload) {


        let filteredNodes = nodes.filter(function(node) {
            let level = getDepthLevel(node.id)

            if (level >= payload.min_depth) {
                return true
            }

        })

        nodes = filteredNodes

    }

    if (('ids' in payload) && payload.ids instanceof Array) {
        console.log('ids->', payload.ids)
        let filteredNodes = nodes.filter(function(node) {
            console.log('node.id->', node.id)
            if (payload.ids.indexOf(+node.id) !== -1) {
                return true
            }
        })

        nodes = filteredNodes
    }

    if (('root_ids' in payload) && payload.root_ids instanceof Array) {

        let filteredNodes = nodes.filter(function(node) {
            console.log('node.id->', node.id)
            if ((payload.root_ids.indexOf(+node.id) !== -1) && node.children.length) {
                return true
            }
        })

        nodes = filteredNodes
    }

    process.stdout.write(JSON.stringify(nodes, null, '  ') + '\n');

}
