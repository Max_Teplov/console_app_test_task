'use strict'

/*
Created by Max Teplov on 29/6/2017
*/


let nodeController = require('./controllers/nodeController');
    

process.stdin.resume();

process.stdin.on('data', function(chunk) {

    try {
        let str = JSON.parse(chunk.toString())

        if ('add_node' in str) {
            return nodeController.addNode(str.add_node)
        }

        if ('delete_node' in str) {
            return nodeController.deleteNode(str.delete_node)
        }

        if ('move_node' in str) {
            return nodeController.moveNode(str.move_node)
        }

        if ('query' in str) {
            return nodeController.query(str.query)
        }

        throw 'i can not understand what do you want'
        // process.stdout.write('data: ' + chunk);
    } catch (e) {
        // console.error('e->', e)
        return process.stdout.write('{"ok":false}\n\n');
    }

});